use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    /// The hardware crypto device is not ready or not available
    #[error("device not ready or not available")]
    NotReady(#[source] ncrypt::Error),
    /// The key already exists in the key store
    #[error("key exists")]
    KeyExists(#[source] ncrypt::Error),
    /// The key was not found in the key store
    #[error("key not found")]
    KeyNotFound(#[source] ncrypt::Error),
    /// The returned key did not match the expected key type
    #[error("unexpected key type")]
    UnexpectedKeyType,
    /// Decryption failed, are you sure the data is PKCS1 padded?
    #[error("invalid data for decryption")]
    DecryptionFailed,
    /// Internal error
    #[error("internal error")]
    Internal(#[source] ncrypt::Error),
}
